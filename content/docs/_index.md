---
title: "The Book of Brud"
---
## The Book of Brud

The collected writings of Brud consist of books, pamphlets, screeds, interviews, articles, doodles, manuscripts, zines, literary ephemera, and assorted miscellanea. Portions of the Book of Brud have been translated into Polish, Latvian, Lithuanian, German, Dutch, Hindi, Sanskrit, Italian, and Esperanto.
