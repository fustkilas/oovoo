---
title: Ummo by Brud
---

# Ummo

![](/images/ummo.jpg)

Brud's Benevolent Dictator Aditya Mandayam has a new solo musical project. Ummo combines West-Coast modular synthesis like “Arp, Buchla, Serge, Ciat-Lonbarde, and countless nameless inventions that used bananas” with Indian classical singing, creating layers of periphonic feedback, resonance, xenharmonic noise, and Carnatic vocals that he calls “modular folk”. In recent years Mandayam’s practice has taken a turn towards the esoteric Hindu traditions of Tantra. Combining avant-garde performance aesthetics with his studies in classical voice, Mandayam’s operatic oeuvre is a gesamtkunstwerk of sculpture, theater, cinema, music, light, text, sound, and image. Drenched in the Tantric trinity of black, white, and red, Ummo is the High Priest of the Temple of the White Cube.
