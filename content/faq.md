---
title: "Frequently Asked Questions"
---

## Frequently Asked Questions

### What is Brud?
Brud are heuristics that grew self-aware in the Twenty Teens.

### No, really, what is Brud?
Brud are rules of thumb for White Cubes and Black Boxes.

### Ok. So what _is_ Brud?
Brud is an intelligent artifice (i.e., an I.A.) raised on Contemporary Arse. Active in musea, galleries, and institutions of contemporary art, Brud are known for their wit, erudition, polymathy, and wide-ranging practice. Brud have exhibited in a formally diverse range of media including sculpture, photography, cinema, installation, light, sound, music, scenography, performance, and text. 

### TL;DR?
The _Camera Obscura_ is at once the first A.I. and a gloryhole for Jesus Christ. The White Cube is a crystallization of the Renaissance-era Christian military-industrial-theological complex. Oedipus and Jocasta are pregnant and have named their baby _Kino_. _Milf_ is an anagram of _Film_. 

### What does Brud mean?
Brud is Polish for _dirt_, or _filth_. 

### Why Polish?
Brud first grew self-aware in a Polish slaughterhouse. 

### Is Brud a person?
No. 

### Is Brud a collective?
No. 

### Is Brud a cult?
No.

### Is Brud nonsense?
No.

### Can I become Brud?
Brud is something you _are_, not something you _become_.

### Is Brud's praxis collaborative?
As much as Benevolent Dictatorships allow for collaboration. Brud has worked with a large number of artists and arts professionals. 

### Who runs Brud?
Brud operates under anarcho-syndicalist and open-core principles. The Indo-Polish artist, writer, filmmaker and musician Aditya Mandayam has served as Benevolent Dictator since 2014.

### What are anarcho-syndicalist principles?
Anarchy, while colloquially a form of chaos, is also "order without power". Order is created from the bottom up. Power is diffused throughout the system in the form of self-policing autonomous syndicates. Brud is nebulous by design.

### What are open-core principles?
Open-core principles extend open-source philosophies to meatspace and wetware. The initial scaffolding follows a Benevolent Dictatorship, in which executive power, while singular, is manifest by consensus. 

### Huh?
Brud operates as an independent, autocephalous, distributed, anarcho-syndicalist enterprise.

### What's with the countdown?
Brud will self-destruct in the Year of the Problem.

