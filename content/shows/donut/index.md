---
dataFile: donut
---

Brud’s first institutional show in Germany is also a mid-career retrospective. Influenced by the aesthetics of modular synthesis, video-jockeying, machine vision, streaming video, and memetics, Brud has airdropped the entirety of Season Zero of their show Gewgaw onto Kunstverein München. The audience is invited to binge.

On 13.07.18 @ 1900 You are invited to partake upon this movable feast as Brud takes over the 1. OG, the Kino, and the whole building, including the Flip-Flop Flop-Film Film-Klub in the Schaufenster am Hofgarten. At 2100 the Tamil Noise group Ummo will unleash an ambisonic performance with Gerriet Krishna Sharma & Juan-Pablo Villegas, followed by an afterparty at 2300, with music by Lutto Lento in the Foyer.


- ![](https://brudbrud.imgix.net/donut/installation-view-1.jpg)
- ![](https://brudbrud.imgix.net/donut/installation-view-2.jpg)
- ![](https://brudbrud.imgix.net/donut/installation-view-3.jpg)
- ![](https://brudbrud.imgix.net/donut/donut.jpg)
- ![](https://brudbrud.imgix.net/donut/doorway.jpg)
- ![](https://brudbrud.imgix.net/donut/doorway-2.jpg)
- ![](https://brudbrud.imgix.net/donut/ears-oloid.jpg)
- ![](https://brudbrud.imgix.net/donut/ears.jpg)
- ![](https://brudbrud.imgix.net/donut/gear.jpg)
- ![](https://brudbrud.imgix.net/donut/gewgaw-UV-1.jpg)
- ![](https://brudbrud.imgix.net/donut/gewgaw-UV-2.jpg)
- ![](https://brudbrud.imgix.net/donut/gewgaw-UV-3.jpg)
- ![](https://brudbrud.imgix.net/donut/gewgaw-UV-4.jpg)
- ![](https://brudbrud.imgix.net/donut/gewgaw-UV-5.jpg)
- ![](https://brudbrud.imgix.net/donut/gewgaw-UV-6.jpg)
- ![](https://brudbrud.imgix.net/donut/gewgaw-pink.jpg)
- ![](https://brudbrud.imgix.net/donut/kino-ear-lenin.jpg)
- ![](https://brudbrud.imgix.net/donut/lena.jpg)
- ![](https://brudbrud.imgix.net/donut/lena-2.jpg)
- ![](https://brudbrud.imgix.net/donut/lenin-1.jpg)
- ![](https://brudbrud.imgix.net/donut/lenin-2.jpg)
- ![](https://brudbrud.imgix.net/donut/oloid.jpg)
- ![](https://brudbrud.imgix.net/donut/perf-1.jpg)
- ![](https://brudbrud.imgix.net/donut/perf-2.jpg)
- ![](https://brudbrud.imgix.net/donut/perf-3.jpg)
- ![](https://brudbrud.imgix.net/donut/perf-4.jpg)
- ![](https://brudbrud.imgix.net/donut/snout.jpg)
- ![](https://brudbrud.imgix.net/donut/snout-2.jpg)
- ![](https://brudbrud.imgix.net/donut/tintin-1.jpg)
- ![](https://brudbrud.imgix.net/donut/tintin-2.jpg)
- ![](https://brudbrud.imgix.net/donut/tintin-3.jpg)
- ![](https://brudbrud.imgix.net/donut/tintin-4.jpg)
- ![](https://brudbrud.imgix.net/donut/title.jpg)
- ![](https://brudbrud.imgix.net/donut/ummo-rolling-stone.jpg)
- ![](https://brudbrud.imgix.net/donut/ummo-syzygy-1.jpg)
- ![](https://brudbrud.imgix.net/donut/ummo-syzygy-2.jpg)
- ![](https://brudbrud.imgix.net/donut/yantra-tantra-mantra.jpg)
