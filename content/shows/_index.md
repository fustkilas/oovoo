---
---

Brud's operatic oeuvre is a _gesamtkunstwerk_ of sculpture, theater, cinema, music, light, text, sound, and (moving) image. 