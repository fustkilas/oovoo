-- sun wed fri mon tue thu sat

module Main exposing (..)

import Array
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)

type alias Day =
    { name: String
    , abbrev: String
    , symbol: String
    , deity: String
    }


sun = Day "Sunday"      "Sun"   "☉"     "Sun"
mon = Day "Monday"      "Mon"   "☽︎"     "Moon"
tue = Day "Tuesday"     "Tue"   "♂"     "Mars"
wed = Day "Wednesday"   "Wed"   "☿"     "Mercury"
thu = Day "Thursday"    "Thu"   "♃"     "Thor"
fri = Day "Friday"      "Fri"   "♀"     "Frigga"
sat = Day "Saturday"    "Sat"   "♄"     "Saturn"


week = [ sun, wed, fri, mon, tue, thu, sat ]
weekArray = Array.fromList week



alphabet = [ "أ", "ب", "ج", "د", "ه", "و", "ز", "ح", "ط", "ي", "ك", "ل", "م", "ن", "ص", "ع", "ف", "ض", "ق", "ر", "س", "ت", "ث", "خ", "ذ", "ظ", "غ", "ش" ]
alphabetArray = Array.fromList alphabet



dayView: Int -> Html msg
dayView day =
    li [] [ text ((Array.get day alphabetArray) |> Maybe.withDefault "")]
    --li [] [ text (List.map .name )]
    --li [] [ text (String.fromInt(day + 1)) ]


main = 

    ul [ id "calendar" ] (

        List.map (\i -> dayView i ) ( List.range 0 27 )

    )

