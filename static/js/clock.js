jQuery(document).ready(function() {
        
    setInterval( function() {

        var seconds = new Date().getSeconds();
        var secdeg = seconds * 6;
        var secrot = "rotate(" + secdeg + "deg)";
        jQuery(".sec").css({"-moz-transform" : secrot, "-webkit-transform" : secrot});
    }, 1000 );

    setInterval( function() {
        var hours = new Date().getHours();
        var mins = new Date().getMinutes();
        var hourdeg = hours * 15 + (mins / 4);
        var hourrot = "rotate(" + hourdeg + "deg)";
        jQuery(".hour").css({"-moz-transform" : hourrot, "-webkit-transform" : hourrot});
    }, 1000 );

    setInterval( function() {
        var mins = new Date().getMinutes();
        var mindeg = mins * 6;
        var minrot = "rotate(" + mindeg + "deg)";
        jQuery(".min").css({"-moz-transform" : minrot, "-webkit-transform" : minrot});
    }, 1000 );
});
